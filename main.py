# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
flights = pd.read_csv("flights.csv.gz")
weather = pd.read_csv('weather.csv.gz')

df = flights.groupby(['carrier','month'])['distance'].agg('sum').reset_index()
fig1 = px.line(df, x="month", y="distance", color="carrier",
              labels={'month':'Month(1-12)', 'distance': 'Total Flight Distance'}, markers=True,
              height=550)

humid = weather[['year', 'month', 'humid_EWR', 'humid_JFK', 'humid_LGA']]
humid = humid.groupby(['year','month'])[['humid_EWR', 'humid_JFK', 'humid_LGA']].agg('mean')
humid.rename(columns={'humid_EWR':'EWR', 'humid_JFK':'JFK', 'humid_LGA':'LGA'}, inplace=True)
humid = humid.stack().to_frame('humid')
humid.reset_index(inplace=True)
humid.rename(columns={'level_2':'origin'}, inplace=True)
dep_delay = flights[flights['dep_delay']>0].groupby(['year', 'month', 'origin']).size().to_frame('flight count')
dep_delay.reset_index(inplace=True)
df2 = pd.merge(humid, dep_delay, on=['year','month','origin'])
fig2 = px.scatter(df2, x="humid", y="flight count",
                 color="origin", height=500,
                labels={'humid': 'Monthly Averge Humidity', 'flight count': 'Delayed Flight Count'}
                 )


app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.H2(children='''
        Fig1: Total flights distance of each flight carrier in each month.
    '''),

    dcc.Graph(
        id='first-graph',
        figure=fig1
    ),

    html.Div(children='''
        From this line chat, we can observe the monthly total flight distance of each flight carrier. 
        We can also compare the total flight distances of different carriers, for example, it can be observed that generally carrier UA flies the farthest in each month.
    '''),

    html.H2(children='''
        Fig2: Delayed Flight Count v.s. the Monthly Average Humidity.
    '''),

    dcc.Graph(
        id='second-graph2',
        figure=fig2
    ),

    html.Div(children='''
        The scatter plot shows a potential trend that generally the higher the humidity, the more flights would delay.
        ''')
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
